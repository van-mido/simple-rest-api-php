<?php


        class DB
        {

                // Credenciales
                private $config = [

                      'DB_NAME' => 'blog_api',
                      'DB_USER' => 'root',
                      'DB_PASS' => ''

                ];

                private $pdo;

                function __construct()
                {
                    
                        try {
                            
                            $this->pdo = new PDO('mysql:host=localhost;dbname=' . $this->config['DB_NAME'], $this->config['DB_USER'], 
                                                $this->config['DB_PASS'],

                                                        [PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION]
                                                );

                            return $this->pdo;                    

                        } catch (PDOException $e) {
                            
                            die('No se pudo conectar con la DB: ' . $e->getMessage());

                        }
                }

                // Metodo para query sin parametros
                function show_result($query)
                {

                        try {
                                //
                                $result = $this->pdo->query($query);

                                return ($result->rowCount() > 0) ? $result : false;


                        } catch (PDOException $e) {

                                return false;
                        }               

                }

                function query_std($query, $bindings)
                {

                       $stmt = $this->pdo->prepare($query);
                       $stmt->execute($bindings); // array[]

                       return ($stmt->rowCount() > 0) ? $stmt : false;
                       
                }

        }


        // $connection = new DB();

        // if ($connection) {


        // //     echo "<h1>Connected</h1>";

        //         $consult = $connection->show_result('SELECT categories.name AS category_name, post.id, post.category_id, post.title, post.body, post.author, post.date FROM post LEFT JOIN categories ON category_id = categories.id ORDER BY post.date DESC');

        //         // var_dump($consult);

        //         while ($row = $consult->fetch()) {

        //                 print_r($row);
        //         }



        // }