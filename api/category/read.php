<?php


        header('Access-Control-Allow-Origin: *');
        header('Content-Type: application/json');


        require_once '../../config/DB.php';

        // Creamos la instancia de conexion de bases de datos
        $conn = new DB();
 
        $categories = $conn->show_result('SELECT id, name FROM categories ORDER BY date DESC');
  
        // Array de almacenamiento, Array anidado
        $data_json['data'] = [];
        
        if ($categories) {

                foreach ($categories as $category) {

                        // var_dump($post);
                        // print_r($post);
                        // echo $post['author'];

                        $category_data = [

                                         'id' => $category['id'],
                                         'name' => $category['name']                         

                                        ];

                        array_push($data_json['data'], $category_data);                     
                }

                echo json_encode($data_json);


        } else {

                echo json_encode(['message' => 'No posts found']);
        }
        




