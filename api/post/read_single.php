<?php


        header('Access-Control-Allow-Origin: *');
        header('Content-Type: application/json');


        require_once '../../config/DB.php';
        require_once '../../models/Post.php';

        // Creamos la instancia de conexion de bases de datos
        $conn = new DB();

        // Optenemos el id desde la URL
        $id = isset($_GET['id']) ? $_GET['id'] : die();

        $post_single = $conn->query_std('SELECT categories.name AS category_name, post.id, post.category_id, post.title, 
                                         post.body, post.author, post.date FROM post LEFT JOIN categories ON 
                                        category_id = categories.id 
                                        WHERE post.id = :id LIMIT 0,1', 
                                        ['id' => $id]
                                    );
                
        
        foreach ($post_single as $post) {

            $post_data = [
                                    'id' => $post['id'],
                                    'title' => $post['title'],
                                    'body' => $post['body'],
                                    'author' => $post['author'],
                                    'category_id' => $post['category_id'],
                                    'category_name' => $post['category_name'],

                        ];
        }
        
        

        print_r(json_encode($post_data));


        