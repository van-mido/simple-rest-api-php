<?php


            // Para delete de registro se colocan dos encabezados
            header('Access-Control-Allow-Origin: *');
            header('Content-Type: application/json');
            header('Access-Control-Allow-Methods: DELETE');
            header('Access-Control-Allow-Headers: Access-Control-Allow-Headers, Content-Type, Access-Control-Allow-Methods, Authorization, X-Requested-With');

            // Source DB connection & functions    
            require_once '../../config/DB.php';
            

            // Creamos la instancia de conexion de bases de datos
            $conn = new DB();

            // Crea un objeto mediante el request
            $info = json_decode(file_get_contents('php://input'));

            $id = htmlspecialchars(strip_tags($info->id));



            $delete_data = $conn->query_std('DELETE FROM post WHERE id = :id',
                                            [
                                                'id' => $id
                                            ]    
                                        );

            if ($delete_data) {

                    echo json_encode(['message' => 'Post deleted']);

            } else {

                    echo json_encode(['message' => 'Post not deleted']);
            }          


            // Postman param
            // Request type: PUT
            // Header: Key = Content-Type
            // Header: Value = application/json
            // Body: raw: colocamos los datos en formato JSON, WARNING! NO SE DEBEN COLOCAR EN LA URL