<?php


        header('Access-Control-Allow-Origin: *');
        header('Content-Type: application/json');


        require_once '../../config/DB.php';

        // Creamos la instancia de conexion de bases de datos
        $conn = new DB();
 
        $posts = $conn->show_result('SELECT categories.name AS category_name, post.id, post.category_id, post.title, 
                              post.body, post.author, post.date FROM post LEFT JOIN categories ON category_id = categories.id 
                              ORDER BY post.date DESC
                             ');


        
        // Array de almacenamiento
        $data_json['data'] = [];
        
        if ($posts) {

                foreach ($posts as $post) {

                        // var_dump($post);
                        // print_r($post);
                        // echo $post['author'];

                        $post_data = [

                                         'id' => $post['id'],
                                         'title' => $post['title'],
                                         'body' => html_entity_decode($post['body']),
                                         'author' => $post['author'],
                                         'category_id' => $post['category_id'],
                                         'category_name' => $post['category_name']

                                        ];

                        array_push($data_json['data'], $post_data);                     
                }

                echo json_encode($data_json);


        } else {

                echo json_encode(['message' => 'No posts found']);
        }
        




