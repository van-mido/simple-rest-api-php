<?php


        // Para la creacion de registro se colocan dos encabezados
        header('Access-Control-Allow-Origin: *');
        header('Content-Type: application/json');
        header('Access-Control-Allow-Methods: POST');
        header('Access-Control-Allow-Headers: Access-Control-Allow-Headers, Content-Type, Access-Control-Allow-Methods, Authorization, X-Requested-With');

        // Source DB connection & functions
        require_once '../../config/DB.php';
        

        // Creamos la instancia de conexion de bases de datos
        $conn = new DB();

        // Crea un objeto de la peticion
        $info = json_decode(file_get_contents('php://input'));

        $title = htmlspecialchars(strip_tags($info->title));
        $body = htmlspecialchars(strip_tags($info->body));
        $author = htmlspecialchars(strip_tags($info->author));
        $category_id = htmlspecialchars(strip_tags($info->category_id));


        $insert_data = $conn->query_std('INSERT INTO post (title, body, author, category_id) 
                                         VALUES (:title, :body, :author, :category_id)',
                                         [
                                                 'title' => $title,
                                                 'body' => $body,
                                                 'author' => $author,
                                                 'category_id' => $category_id
                                         ]
                                        );

        if ($insert_data) {

                echo json_encode(['message' => 'Post Created']);

        } else {

                echo json_encode(['message' => 'Post not created']);
        }                                

        // Postman param
        // Request type: POST
        // Header: Key = Content-Type
        // Header: Value = application/json
        // Body: raw: colocamos los datos en formato JSON, WARNING! NO SE DEBEN COLOCAR EN LA URL


        // $title = $_GET['title'] ?? '';
        // $title = htmlspecialchars($title);



        // $query = 'UPDATE SET title = :title, body = :body, author = :author, category_id = :category_id WHERE id = :id';
