<?php

        // Para update de registro se colocan dos encabezados
        header('Access-Control-Allow-Origin: *');
        header('Content-Type: application/json');
        header('Access-Control-Allow-Methods: UPDATE');
        header('Access-Control-Allow-Headers: Access-Control-Allow-Headers, Content-Type, Access-Control-Allow-Methods, Authorization, X-Requested-With');


        require_once '../../config/DB.php';
        

        // Creamos la instancia de conexion de bases de datos
        $conn = new DB();

        // Crea un objeto de la peticion
        $info = json_decode(file_get_contents('php://input'));

   
        $title = htmlspecialchars(strip_tags($info->title));
        $body = htmlspecialchars(strip_tags($info->body));
        $author = htmlspecialchars(strip_tags($info->author));
        $category_id = htmlspecialchars(strip_tags($info->category_id));
        $id = htmlspecialchars(strip_tags($info->id));



        $update_data = $conn->query_std('UPDATE post SET  title = :title, body = :body, author 
                                        = :author, category_id = :category_id WHERE id = :id',
                                        [
                                            'title' => $title,
                                            'body' => $body,
                                            'author' => $author,
                                            'category_id' => $category_id,
                                            'id' => $id
                                        ]    
                                    );

        if ($update_data) {

                echo json_encode(['message' => 'Post updated']);

        } else {

                echo json_encode(['message' => 'Post not updated']);
        }          
        
        
        // Postman param
        // Request type: PUT
        // Header: Key = Content-Type
        // Header: Value = application/json
        // Body: raw: colocamos los datos en formato JSON, WARNING! NO SE DEBEN COLOCAR EN LA URL