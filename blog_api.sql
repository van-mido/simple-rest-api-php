-- MariaDB dump 10.19  Distrib 10.4.20-MariaDB, for Win64 (AMD64)
--
-- Host: localhost    Database: blog_api
-- ------------------------------------------------------
-- Server version	10.4.20-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `categories`
--

DROP TABLE IF EXISTS `categories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `categories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) DEFAULT NULL,
  `date` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `categories`
--

LOCK TABLES `categories` WRITE;
/*!40000 ALTER TABLE `categories` DISABLE KEYS */;
INSERT INTO `categories` VALUES (1,'Technology','2021-09-15 19:42:27'),(2,'Gaming','2021-09-15 19:42:36'),(3,'Auto','2021-09-15 19:42:41'),(4,'Entertainment','2021-09-15 19:42:53'),(5,'Books','2021-09-15 19:43:04');
/*!40000 ALTER TABLE `categories` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `post`
--

DROP TABLE IF EXISTS `post`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `post` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `category_id` int(11) DEFAULT NULL,
  `title` varchar(100) DEFAULT NULL,
  `body` text DEFAULT NULL,
  `author` varchar(100) DEFAULT NULL,
  `date` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `post`
--

LOCK TABLES `post` WRITE;
/*!40000 ALTER TABLE `post` DISABLE KEYS */;
INSERT INTO `post` VALUES (1,2,'Post 1','Lorem ipsum dolor sit amet consectetur adipisicing elit. Modi ullam iusto debitis fugit id officia unde quasi ducimus consequatur, cum ipsa? Sapiente iure voluptate animi labore maxime enim reiciendis aspernatur?','VanMido','2021-09-18 18:47:21'),(2,2,'Post 2','Lorem ipsum dolor sit amet consectetur adipisicing elit. Modi ullam iusto debitis fugit id officia unde quasi ducimus consequatur, cum ipsa? Sapiente iure voluptate animi labore maxime enim reiciendis aspernatur?','VanMido','2021-09-18 18:47:36'),(3,3,'Post 3','Lorem ipsum dolor sit amet consectetur adipisicing elit. Modi ullam iusto debitis fugit id officia unde quasi ducimus consequatur, cum ipsa? Sapiente iure voluptate animi labore maxime enim reiciendis aspernatur?','VanMido','2021-09-18 18:48:25'),(4,3,'Cool post by me','Blablabla','VanMido','2021-09-22 21:33:43'),(5,1,'Post 5','Lorem ipsum dolor sit amet consectetur adipisicing elit. Modi ullam iusto debitis fugit id officia unde quasi ducimus consequatur, cum ipsa? Sapiente iure voluptate animi labore maxime enim reiciendis aspernatur?','VanMido','2021-09-18 18:49:01'),(6,1,'Post 6','Lorem ipsum dolor sit amet consectetur adipisicing elit. Modi ullam iusto debitis fugit id officia unde quasi ducimus consequatur, cum ipsa? Sapiente iure voluptate animi labore maxime enim reiciendis aspernatur?','VanMido','2021-09-18 18:49:17'),(7,4,'Post 7','Lorem ipsum dolor sit amet consectetur adipisicing elit. Modi ullam iusto debitis fugit id officia unde quasi ducimus consequatur, cum ipsa? Sapiente iure voluptate animi labore maxime enim reiciendis aspernatur?','Steve O','2021-09-18 18:50:04'),(8,4,'Post 8','Lorem ipsum dolor sit amet consectetur adipisicing elit. Modi ullam iusto debitis fugit id officia unde quasi ducimus consequatur, cum ipsa? Sapiente iure voluptate animi labore maxime enim reiciendis aspernatur?','Steve O','2021-09-18 18:50:16'),(10,5,'Great post','Lorem, ipsum dolor sit amet consectetur adipisicing elit. Ipsa numquam sint molestiae quisquam, debitis architecto sequi amet dolorum itaque neque unde minus adipisci ut tempore quo? Soluta cum excepturi blanditiis.','VanMido','2021-09-21 21:32:54'),(11,4,'Great new post','Lorem ipsum dolor sit amet consectetur, adipisicing elit. Obcaecati eligendi nam debitis cumque quia numquam! Amet distinctio, ducimus aliquid soluta eligendi cumque, aperiam, nihil debitis quibusdam corrupti accusamus. Numquam, ipsam?','Ricardo Vargas','2021-09-22 19:15:06'),(12,2,'Post about E-Sports','Lorem ipsum dolor sit amet consectetur, adipisicing elit. Obcaecati eligendi nam debitis cumque quia numquam! Amet distinctio, ducimus aliquid soluta eligendi cumque, aperiam, nihil debitis quibusdam corrupti accusamus. Numquam, ipsam?','Shinigami','2021-09-22 19:17:36'),(13,2,'Fornite Championship','Lorem ipsum dolor sit amet consectetur, adipisicing elit. Obcaecati eligendi nam debitis cumque quia numquam! Amet distinctio, ducimus aliquid soluta eligendi cumque, aperiam, nihil debitis quibusdam corrupti accusamus. Numquam, ipsam?','VanMido','2021-09-22 19:48:12');
/*!40000 ALTER TABLE `post` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-09-24 11:53:51
